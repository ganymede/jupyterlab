FROM python:3.8

RUN git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it && \
  bash ~/.bash_it/install.sh --silent

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
  apt-get upgrade -y && \
  apt-get install -y nodejs texlive texlive-science texlive-latex-extra texlive-xetex dvipng man-db cm-super  graphviz && \
  rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-1-amd64.deb && dpkg -i pandoc-2.10.1-1-amd64.deb && rm -f pandoc-2.10.1-1-amd64.deb


RUN python -m pip install --upgrade pip && \
  python -m pip install --use-feature=2020-resolver --upgrade \
    jupyterlab \
    ipywidgets \
    jupyterlab_latex \
    plotly \
    sphinx \
    recommonmark \
    nbsphinx  \
    sphinx_rtd_theme \
    bokeh \
    numpy \
    scipy \
    numexpr \
    patsy \
    scikit-learn \
    scikit-image \
    matplotlib \
    ipython \
    pandas \
    sympy \
    seaborn \
    nose \
#    jupyterlab-git \
#    jupyter-lsp \
#    python-language-server \
    ipytest \
    coverage \
    h5py \
#    ipyvolume \
#    pyvis \
#    holoext \
#    hvplot \
#    bqplot \
#    ipyvuetify \
#    voila \
#    voila-vuetify \
    lolviz \
    graphviz \
#    tabulate \
#    altair \
#    vega_datasets \
    ipympl && \
  jupyter labextension install \
    @jupyter-widgets/jupyterlab-manager \
    jupyterlab-plotly \
#    @bokeh/jupyter_bokeh \
#    @jupyterlab/git \
#    @krassowski/jupyterlab-lsp \
    @jupyterlab/toc 
#    jupyter-vuetify \
#    bqplot \
#    ipyvolume \
#    jupyter-threejs

# Install Science Plots
RUN python -m pip install git+https://github.com/garrettj403/SciencePlots.git

COPY bin/entrypoint.sh /usr/local/bin/
COPY config/ /root/.jupyter/

EXPOSE 8888
VOLUME /notebooks
WORKDIR /notebooks
ENTRYPOINT ["entrypoint.sh"]

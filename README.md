[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](LICENSE)

This docker image has been forged from https://github.com/vemonet/Jupyterlab (Copyright (c) 2019 Alexander Malic)

## Jupyterlab Docker container

**This Docker container runs as root user!** It can be helpful when e.g. the popular jupyter/datascience-notebook image does not work because it runs as Jovyan user. 

#### Installed Jupyterlab extensions
- [Jupyter Widgets](https://ipywidgets.readthedocs.io/en/latest/examples/Widget%20Basics.html)
- [@jupyterlab/latex](https://github.com/jupyterlab/jupyterlab-latex)
- [jupyterlab-plotly](https://www.npmjs.com/package/jupyterlab-plotly)
- [jupyterlab-drawio](https://github.com/QuantStack/jupyterlab-drawio)
- [jupyterlab-spreadsheet](https://github.com/quigleyj97/jupyterlab-spreadsheet)
- [@bokeh/jupyter_bokeh](https://github.com/bokeh/jupyter_bokeh)
- [@jupyterlab/toc](https://www.npmjs.com/package/@jupyterlab/toc)
- [@jupyterlab/git](https://www.npmjs.com/package/@jupyterlab/git)
- [@krassowski/jupyterlab-lsp](https://github.com/krassowski/jupyterlab-lsp)
- [jupyter-vuetify](https://github.com/mariobuikhuizen/ipyvuetify)

### Your notebooks

Volumes can be mounted into `/notebooks` folder. If the folder contains a requirements.txt file, it will be installed automatically when the container starts up.

---

### Pull/Update to latest version
```bash
docker pull registry.gitlab.com/ganymede/jupyterlab:latest
```

### Run

The container will install requirements from files present at the root of the repository at `docker run` (in this order):

* `packages.txt`: install apt-get packages
* `requirements.txt`: install pip packages
* `extensions.txt`: install Jupyterlab extensions

### Run from Git repository

The jupyter lab is executed inside docker using `docker run`:

#### Example (Plain):
```bash
docker run --rm -it -p 8888:8888 registry.gitlab.com/ganymede/jupyterlab
```

or if you want to define your own password
```bash
docker run --rm -it -p 8888:8888 -e PASSWORD="<your_secret>" registry.gitlab.com/ganymede/jupyterlab:latest
```

You can also provide data in `/notebooks`:

#### Example (Mount volume):
```bash
docker run --rm -it -p 8888:8888 -v /data/jupyterlab-notebooks:/notebooks -e PASSWORD="<your_secret>" registry.gitlab.com/ganymede/jupyterlab:latest
```

#### Example (Downloading a git repository and installing dependencies):
```bash
docker run --rm -it -p 8888:8888 -e PASSWORD="<your_secret>" -e GIT_URL="https://gitlab.com/ganymede/general_relativity.git" registry.gitlab.com/ganymede/jupyterlab:latest
```

* Access the jupyterlab on http://localhost:8888
* Can also be used with the jupyter notebook interface: http://localhost:8888/tree/notebooks


or use the current directory as source code in the container:

```bash
docker run --rm -it -p 8888:8888 -v $(pwd):/notebooks -e PASSWORD="<your_secret>" registry.gitlab.com/ganymede/jupyterlab:latest
```

> Use `${pwd}` for Windows

### Build from source

```bash
docker build -t registry.gitlab.com/ganymede/jupyterlab .
```
